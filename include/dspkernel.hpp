#pragma once

#include <string>
#include <vector>
#include <map>
#include <queue>
#include <functional>

namespace dsp {
	constexpr int VERSION = 4;

	constexpr int arrayBit = 0x10000;

	enum ParameterType {
		FLOAT = 1,
		DOUBLE = 2,
		FLOAT_ARRAY = arrayBit | FLOAT,
		DOUBLE_ARRAY = arrayBit | DOUBLE
	};

	class Kernel;
	class KeyboardEventHandler;

	typedef float Sample;
	typedef int Offset;

	template<class T>
	using ParameterHandler = std::function<void(T)>;

	template<class T>
	using ArrayElementParameterHandler = std::function<void(unsigned, T)>;

	template<class T>
	struct Array {
		Array(int elements, T* values): elements(elements), values(values) {}

		unsigned elements;
		T* values;

		/**
		 * @brief Creates copy of array.
		 */
		inline static Array from(const Array &array) {
			return Array::from(array.elements, array.values);
		}

		/**
		 * @brief Creates copy of array.
		 */
		inline static Array from(int elements, const T* values) {
			T* newValues = new T[elements];

			std::memcpy(newValues, values, elements * sizeof(T));

			return Array(elements, newValues);
		}

		inline void free() {
			delete[] values;
		}
	};

	class Kernel {
		struct Event {
			Event(Offset offset, std::function<void()> task): offset(offset), task(task) {}

			Offset offset;
			std::function<void()> task;

			inline bool operator < (const Event& e) const {
				return offset < e.offset;
			}
		};

		std::map<int, ParameterHandler<float>> floatParametersHandlers;
		std::map<int, ParameterHandler<double>> doubleParametersHandlers;
		std::map<int, ParameterHandler<Array<float>>> floatArrayParametersHandlers;
		std::map<int, ParameterHandler<Array<double>>> doubleArrayParametersHandlers;
		std::map<int, ArrayElementParameterHandler<float>> floatArrayElementParametersHandlers;
		std::map<int, ArrayElementParameterHandler<double>> doubleArrayElementParametersHandlers;
		std::vector<std::string> parametersNames;
		std::vector<ParameterType> parametersTypes;
		std::priority_queue<Event> eventsQueue;
	public:
		virtual ~Kernel() {}

		/**
		 * Any change to API is a breaking change and should increase VERSION.
		 */
		virtual const int dspKernelVersion() const = 0;
		virtual const char* getName() const = 0;

		virtual void configurePlayback(double sampleRate, int inputChannels, int outputChannels, int blockSize) = 0;
		virtual void process(const Sample* inputs, Sample* outputs) = 0;

		virtual void noteOn(int offset, int voice, unsigned key, float value) = 0;
		virtual void noteOff(int offset, int voice) = 0;

		inline void addEvent(Offset offset, std::function<void()> task) {
			eventsQueue.emplace(offset , task);
		}

		inline int getParametersNumber() const {
			return parametersNames.size();
		}

		inline const char* getParameterName(int id) const {
			return parametersNames[id].c_str();
		}

		inline ParameterType getParameterType(int id) const {
			return parametersTypes[id];
		}

		inline void setParameter(int id, float value) {
			floatParametersHandlers[id](value);
		}

		inline void setParameter(int id, double value) {
			doubleParametersHandlers[id](value);
		}

		/**
		 * @brief Updates the array parameter. Copy of provided parameter is used.
		 */
		inline void setParameter(int id, const Array<float> array) {
			floatArrayParametersHandlers[id](Array<float>::from(array));
		}

		/**
		 * @brief Updates the array parameter. Copy of provided parameter is used.
		 */
		inline void setParameter(int id, const Array<double> array) {
			doubleArrayParametersHandlers[id](Array<double>::from(array));
		}

		/**
		 * @brief Updates value of array element by index.
		 */
		inline void setParameter(int id, unsigned index, float value) {
			floatArrayElementParametersHandlers[id](index, value);
		}

		/**
		 * @brief Updates value of array element by index.
		 */
		inline void setParameter(int id, unsigned index, double value) {
			doubleArrayElementParametersHandlers[id](index, value);
		}
	protected:
		inline void processTasksQueue(Offset toOffset, std::function<void()> task) {
			while(!eventsQueue.empty()) {
				Event event = eventsQueue.top();

				if (event.offset > toOffset) {
					return;
				}

				event.task();

				eventsQueue.pop();
			}
		}

		inline void addFloatParameter(const char* name, ParameterHandler<float> handler) {
			floatParametersHandlers[assignParameterId(name, ParameterType::FLOAT)] = handler;
		}

		inline void addDoubleParameter(const char* name, ParameterHandler<double> handler) {
			doubleParametersHandlers[assignParameterId(name, ParameterType::DOUBLE)] = handler;
		}

		inline void addFloatArrayParameter(
			const char* name,
			ParameterHandler<Array<float>> arrayHandler,
			ArrayElementParameterHandler<float> elementHandler
		) {
			int id = assignParameterId(name, ParameterType::FLOAT_ARRAY);

			floatArrayParametersHandlers[id] = arrayHandler;
			floatArrayElementParametersHandlers[id] = elementHandler;
		}

		inline void addDoubleArrayParameter(
			const char* name,
			ParameterHandler<Array<double>> arrayHandler,
			ArrayElementParameterHandler<double> elementHandler
		) {
			int id = assignParameterId(name, ParameterType::DOUBLE_ARRAY);

			doubleArrayParametersHandlers[id] = arrayHandler;
			doubleArrayElementParametersHandlers[id] = elementHandler;
		}
	private:
		inline int assignParameterId(const std::string& name, ParameterType type) {
			int id = parametersNames.size();

			parametersNames.push_back(name);
			parametersTypes.push_back(type);

			return id;
		}

		// TODO: Add "diagnostic" output buffers
	};

	inline const char* parameterTypeString(dsp::ParameterType parameterType) {
		switch (parameterType) {
			case dsp::ParameterType::FLOAT:
				return "float";
			case dsp::ParameterType::DOUBLE:
				return "double";
			case dsp::ParameterType::FLOAT_ARRAY:
				return "float[]";
			case dsp::ParameterType::DOUBLE_ARRAY:
				return "double[]";
			default:
				return "unknown";
		}
	}
}